.data
operador1:	.word		4, 0xFFFFFFFF, 0, 0xFFFFFFFF, 0xFFFFFFFF
operador2:	.word		3, 1, 1, 0
resultado:	.space		260
tamano:		.asciiz 	"Tamano: "
nlinea:		.asciiz 	"\n"
enteros:	.asciiz 	"Enteros: "

.text
main:
	la		$a0, operador1
	la		$a1, operador2
	la		$a2, resultado
	jal		sumarEnterosLargos
	move		$a0, $a2
	jal		imprimirEnteroLargo
	j 		salida

sumarEnterosLargos:
	lw		$t0, 0($a0)
	lw		$t1, 0($a0)			#Carga las longitudes
	bgt		$t0, $t1, segundoMayor
	add		$s0, $0, $t1
	j		comparacionCompleta
segundoMayor:
	add		$s0, $0, $t0			#Establece el limite del ciclo
comparacionCompleta:
	add		$s1, $0, $0 			#Inicializa el almacenamiento para el acarreo
	addi		$a0, $a0, 0x00000004 		#Mueve los punteros al inicio de los arreglos
	addi		$a1, $a1, 0x00000004 		#Por ahora no me interesa optimizar
	add		$s2, $a2, $0 			#Copia del puntero al tamano del enteroLargo resultante
	addi		$a2, $a2, 0x00000004
	add		$s3, $0, $0			#Inicializa el contador del loop
inicioCiclo:
	lw		$t0, 0($a0)
	lw		$t1, 0($a1) 			#Carga los enteros	
	beqz		$t0, noAcarreo
	beqz		$t1, noAcarreo
	not		$t2, $t0 
	addiu		$t2, $t2, 0x00000001		#Complemento a dos del numero
	bltu		$t1, $t2, noAcarreo
	addi		$t3, $0, 0x00000001 		#El acarreo o sucede o no sucede, no tiene un valor numerico
	j		acarreoCalculado     
noAcarreo:
	addi		$t3, $0, 0x00000000
acarreoCalculado:
	addu		$t4, $t0, $t1 			#Suma los enteros
	addu		$t4, $t4, $s1			#Suma el acarreo anterior
	bgtu		$t4, $t0, noAcarreo2		#Si la suma resulta ser menor que alguno de los operadores, hay acarreo
	bgtu		$t4, $t1, noAcarreo2
	addi		$t3, $0, 0x00000001
noAcarreo2:
	add		$s1, $t3, $0			#Actualiza el acarreo
	sw		$t4, 0($a2) 			#Guarda el resultado en la memoria
	addi		$a0, $a0, 0x00000004 		#Avanza los punteros a la siguiente posicion
	addi		$a1, $a1, 0x00000004
	addi		$a2, $a2, 0x00000004
	addi		$s3, $s3, 0x00000001 		#Aumenta el contador
	blt		$s3, $s0, inicioCiclo
	beqz		$s1, guardarLongitudNumero	#Si hay acarreo despues del ciclo es necesario abrir una nueva posicion con valor 1
	bge		$s1, 0x00000040, guardarLongitudNumero
	addi		$s3, $s3, 0x00000001 		#No podemos agrandar el numero mas alla de 64
	sw		$s1, 0($a2)
guardarLongitudNumero:
	sw		$s3, 0($s2)
	move		$a2, $s2			#Restaura la copia del puntero
	jr 		$ra

imprimirEnteroLargo:
	move 		$t0, $a0
	lw		$t1, 0($t0)			#Carga el tamano del parametro a imprimir
	move		$t2, $0
	li		$v0, 0x00000004
	la		$a0, tamano
	syscall
	li		$v0, 0x00000001
	move		$a0, $t1
	syscall
	li		$v0, 0x00000004
	la		$a0, nlinea
	syscall
	la		$a0, enteros
	syscall
	li		$v0, 0x00000001
cicloImpresion:
	addi		$t0, $t0, 0x00000004
	lw		$t3, 0($t0)
	move		$a0, $t3
	syscall
	addi		$t2, $t2, 0x00000001
	blt		$t2, $t1, cicloImpresion
	jr		$ra

crearEnteroLargo:
	li		$a0, 260
	li		$v0, 9
	syscall
	move		$t0, $v0
	li		$t1, 0
cicloInicializacion:
	sw		$0, 0($t0)
	addi		$t1, $t1, 1
	addi		$t0, $t0, 4
	bne		$t1, 65, cicloInicializacion
	jal		$ra

salida: